"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FishbowlError = void 0;
const tslib_1 = require("tslib");
const crypto_1 = tslib_1.__importDefault(require("crypto"));
const csv_parse_1 = tslib_1.__importDefault(require("csv-parse"));
const csv_stringify_1 = tslib_1.__importDefault(require("csv-stringify"));
const fishbowl_status_json_1 = tslib_1.__importDefault(require("./static/fishbowl-status.json"));
const logger_1 = tslib_1.__importDefault(require("./logger"));
const client_1 = require("./client");
const requests_1 = require("./requests");
const responses_1 = require("./responses");
/**
 * Catch-all class for any error from Fishbowl.
 */
class FishbowlError {
    constructor(statusCode, message = '', param = '') {
        this.statusCode = statusCode;
        this.message = message;
        this.param = param;
    }
    toString() {
        const repr = `<${this.statusCode}>: ${this.message}`;
        return repr.replace(FishbowlError.template, this.param);
    }
}
exports.FishbowlError = FishbowlError;
FishbowlError.template = '_________';
FishbowlError.statusCodes = fishbowl_status_json_1.default;
class Fishbowl {
    constructor({ iaId = 54321, iaName = 'fishbowlTS', iaDescription = 'fishbowlTS helper', username = 'admin', password = 'admin', host, port, }) {
        this.key = '';
        this.userId = 0;
        this._authenticated = false;
        this.iaId = iaId;
        this.iaName = iaName;
        this.iaDescription = iaDescription;
        this.username = username;
        this.password = password;
        this.client = new client_1.FishbowlClient(host, port);
    }
    /**
     * Getter for login status.
     */
    get authenticated() {
        return this._authenticated;
    }
    /**
     * Import data into Fishbowl.
     */
    import(request) {
        var e_1, _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got import() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            const row = [];
            const stringifier = csv_stringify_1.default({
                header: true
            });
            stringifier.write(request.row);
            stringifier.end();
            try {
                for (var stringifier_1 = tslib_1.__asyncValues(stringifier), stringifier_1_1; stringifier_1_1 = yield stringifier_1.next(), !stringifier_1_1.done;) {
                    const csvRecord = stringifier_1_1.value;
                    row.push(csvRecord);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (stringifier_1_1 && !stringifier_1_1.done && (_a = stringifier_1.return)) yield _a.call(stringifier_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.IMPORT, responses_1.Responses.Name.IMPORT, {
                Type: request.type,
                Rows: {
                    Row: row
                }
            });
            logger_1.default.info('Import successful! Got response from Fishbowl Server:');
            logger_1.default.info(JSON.stringify(response));
            return response;
        });
    }
    /**
     * Get headers for a specific import.
     */
    getImportHeaders(request) {
        var e_2, _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got getImportHeaders() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            let response = yield this._sendRequest(requests_1.Requests.Name.IMPORT_HEADERS, responses_1.Responses.Name.IMPORT_HEADERS, {
                Type: request.type
            });
            logger_1.default.info('Import headers request successful! Got response from Fishbowl Server:');
            logger_1.default.info(JSON.stringify(response));
            const parser = csv_parse_1.default();
            parser.write(response.Header.Row.join('\n'));
            parser.end();
            const headers = [];
            try {
                for (var parser_1 = tslib_1.__asyncValues(parser), parser_1_1; parser_1_1 = yield parser_1.next(), !parser_1_1.done;) {
                    const header = parser_1_1.value;
                    headers.push(header);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (parser_1_1 && !parser_1_1.done && (_a = parser_1.return)) yield _a.call(parser_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
            response.Header.Row = headers;
            return response;
        });
    }
    /**
     * Issue an SO.
     */
    issueSO(request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got issueSO() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.ISSUE_SO, responses_1.Responses.Name.ISSUE_SO, {
                SONumber: request.soNumber
            });
            logger_1.default.info('Issued SO successfully. Got response from Fishbowl Server:');
            logger_1.default.info(JSON.stringify(response));
            return response;
        });
    }
    /**
     * Log into Fishbowl.
     */
    login() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got login() request.');
            if (this._authenticated) {
                logger_1.default.info('Already logged in!');
                return;
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.LOGIN, responses_1.Responses.Name.LOGIN, {
                IAID: this.iaId,
                IAName: this.iaName,
                IADescription: this.iaDescription,
                UserName: this.username,
                UserPassword: crypto_1.default
                    .createHash('md5')
                    .update(this.password)
                    .digest('base64')
            });
            logger_1.default.info('Login successful! Got response from Fishbowl Server:');
            logger_1.default.info(JSON.stringify(response));
            this._authenticated = true;
            return response;
        });
    }
    /**
     * Log out of Fishbowl.
     */
    logout() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got logout() request.');
            const response = yield this._sendRequest(requests_1.Requests.Name.LOGOUT, responses_1.Responses.Name.LOGOUT, '');
            logger_1.default.info('Logout successful.');
            this._authenticated = false;
            return response;
        });
    }
    /**
     * Run a SQL query and get CSV back.
     */
    query(request) {
        var e_3, _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const query = request.query || '';
            logger_1.default.info('Got query() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.QUERY, responses_1.Responses.Name.QUERY, {
                Name: request.name || '',
                Query: query
            });
            logger_1.default.info('Query successful!');
            // Convert any returned CSV to JSON objects keyed by column headers.
            try {
                logger_1.default.info('Got CSV response from Fishbowl Server. Converting to JSON...');
                const parser = csv_parse_1.default({
                    columns: true // `true` will use the first row as headers and map data as objects
                });
                parser.write(response.Rows.Row.join('\n'));
                parser.end();
                const rows = [];
                try {
                    for (var parser_2 = tslib_1.__asyncValues(parser), parser_2_1; parser_2_1 = yield parser_2.next(), !parser_2_1.done;) {
                        const record = parser_2_1.value;
                        rows.push(record);
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (parser_2_1 && !parser_2_1.done && (_a = parser_2.return)) yield _a.call(parser_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
                logger_1.default.info('CSV successfully converted to JSON.');
                response.Rows.Row = rows;
            }
            catch (e) {
                logger_1.default.warn('Error parsing CSV to JSON: ');
                logger_1.default.warn(JSON.stringify(e));
            }
            return response;
        });
    }
    /**
     * Submit a quickship (pick, pack, ship) for an SO.
     */
    quickshipSO(request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got quickshipSO() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.QUICKSHIP_SO, responses_1.Responses.Name.QUICKSHIP_SO, {
                SONumber: request.soNumber,
                shipDate: request.shipDate,
                fulfillServiceItems: !!request.fulfillServices,
                errorIfNotFulfilled: !!request.mustFulfill
            });
            logger_1.default.info('Quickship successful! Got response from Fishbowl Server:');
            logger_1.default.info(JSON.stringify(response));
            return response;
        });
    }
    /**
     * Void out an SO.
     */
    voidSO(request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Got voidSO() request.');
            if (!this._authenticated) {
                yield this.login();
            }
            const response = yield this._sendRequest(requests_1.Requests.Name.VOID_SO, responses_1.Responses.Name.VOID_SO, {
                SONumber: request.soNumber
            });
            logger_1.default.info('Voided SO successfully. Got response from Fishbowl Server: ');
            logger_1.default.info(JSON.stringify(response));
            return response;
        });
    }
    /**
     * Convenience method for dispatching to the client and throwing any errors
     * that occur during the request.
     * @private
     */
    _sendRequest(type, responseType, data) {
        var _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info(`Sending ${type} request to Fishbowl Server...`);
            const body = new requests_1.FBRequestEnvelope(this.key, {
                [type]: data
            });
            let response = yield this.client.send({ body });
            const json = response.FbiJson;
            if (!this.key) {
                this.key = json.Ticket.Key;
            }
            if (!this.userId) {
                this.userId = json.Ticket.UserID;
            }
            logger_1.default.info('Response received from Fishbowl Server.');
            // handle transport, auth or server-level errors
            let status = json.FbiMsgsRs.statusCode;
            logger_1.default.info(`Server status came back as ${status}.`);
            if (status !== Fishbowl.OK) {
                logger_1.default.error(`Fishbowl server error: ${status}`);
                throw new FishbowlError(status, ((_a = json.FbiMsgsRs) === null || _a === void 0 ? void 0 : _a.statusMessage) || FishbowlError.statusCodes[status], type);
            }
            const responseData = json.FbiMsgsRs[responseType];
            status = responseData.statusCode;
            logger_1.default.info(`Request status came back as ${status}.`);
            if (status !== Fishbowl.OK) {
                logger_1.default.error(`Fishbowl request error: ${status}`);
                throw new FishbowlError(status, responseData.statusMessage || FishbowlError.statusCodes[status], type);
            }
            return responseData;
        });
    }
}
exports.default = Fishbowl;
Fishbowl.OK = 1000;
//# sourceMappingURL=index.js.map