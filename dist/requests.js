"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FBRequestEnvelope = exports.Requests = void 0;
/**
 * Interfaces for the request objects our API accepts.
 */
var Requests;
(function (Requests) {
    let Name;
    (function (Name) {
        Name["QUERY"] = "ExecuteQueryRq";
        Name["IMPORT"] = "ImportRq";
        Name["IMPORT_HEADERS"] = "ImportHeaderRq";
        Name["ISSUE_SO"] = "IssueSORq";
        Name["QUICKSHIP_SO"] = "QuickShipRq";
        Name["VOID_SO"] = "VoidSORq";
        Name["LOGIN"] = "LoginRq";
        Name["LOGOUT"] = "LogoutRq";
    })(Name = Requests.Name || (Requests.Name = {}));
})(Requests = exports.Requests || (exports.Requests = {}));
/**
 * The request envelope JSON format for a Fishbowl request. Exported but should
 * not be used directly by the library consumer.
 */
class FBRequestEnvelope {
    constructor(key, request) {
        this.FbiJson = {
            Ticket: {
                Key: key
            },
            FbiMsgsRq: request
        };
    }
}
exports.FBRequestEnvelope = FBRequestEnvelope;
//# sourceMappingURL=requests.js.map