export interface FBResponse {
    statusCode: number;
    statusMessage?: string;
}
export declare namespace Responses {
    interface Login extends FBResponse {
        UserFullName: string;
        ModuleAccess: {
            Module: string[];
        };
        ServerVersion: string;
    }
    interface ImportHeaders extends FBResponse {
        Header: {
            Row: string[];
        };
    }
    interface Query extends FBResponse {
        Rows: {
            Row: string[] | any[];
        };
    }
    enum Name {
        QUERY = "ExecuteQueryRs",
        IMPORT = "ImportRs",
        IMPORT_HEADERS = "ImportHeaderRs",
        ISSUE_SO = "IssueSORs",
        QUICKSHIP_SO = "QuickShipRs",
        VOID_SO = "VoidSORs",
        LOGIN = "LoginRs",
        LOGOUT = "LogoutRs"
    }
}
export interface FBResponseEnvelope {
    FbiJson: {
        Ticket: {
            UserID: number;
            Key: string;
        };
        FbiMsgsRs: {
            statusCode: number;
            statusMessage?: string;
            [Responses.Name.QUERY]?: Responses.Query;
            [Responses.Name.IMPORT]?: FBResponse;
            [Responses.Name.IMPORT_HEADERS]?: Responses.ImportHeaders;
            [Responses.Name.ISSUE_SO]?: FBResponse;
            [Responses.Name.QUICKSHIP_SO]?: FBResponse;
            [Responses.Name.VOID_SO]?: FBResponse;
            [Responses.Name.LOGIN]?: Responses.Login;
            [Responses.Name.LOGOUT]?: FBResponse;
        };
    };
}
