/**
 * A generic interface for an inbound request to a FishbowlClient.
 */
declare namespace FishbowlClient {
    interface Request {
        body?: any;
        json?: boolean;
    }
}
/**
 * All possible states for the connection to FB Server.
 */
export declare enum Status {
    OFFLINE = "offline",
    CONNECTING = "connecting",
    SENDING = "sending",
    WAITING = "waiting",
    READY = "ready",
    ERROR = "error"
}
/**
 * A FishbowlClient holds a socket connection to FB Server and manages queuing
 * and dispatching requests and responses. The client exposes a single async `send`
 * method that accepts a ClientRequest. The returned promise will receive the
 * text server response when fulfilled, or a rehydrated JSON object if the `json`
 * property on the request object was truthy.
 */
export declare class FishbowlClient {
    private _host;
    private _port;
    status: Status;
    private requests;
    private connection;
    private reconnecting;
    constructor(_host?: string, _port?: number);
    /**
     * The only public method. Call with a ClientRequest to receive a Promise awaiting
     * the text response from the FB Server.
     */
    send(request: FishbowlClient.Request): Promise<any>;
    /**
     * Open the socket to FB Server. The Promise returned fulfills in the callback to
     * opening the socket so we can await a confirmed connection before sending data.
     */
    private _connect;
    /**
     * Dispatches a single request from the queue.
     */
    private _dispatch;
}
export {};
