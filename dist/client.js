"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FishbowlClient = exports.Status = void 0;
const tslib_1 = require("tslib");
const net_1 = tslib_1.__importDefault(require("net"));
const logger_1 = tslib_1.__importDefault(require("./logger"));
/**
 * All possible states for the connection to FB Server.
 */
var Status;
(function (Status) {
    Status["OFFLINE"] = "offline";
    Status["CONNECTING"] = "connecting";
    Status["SENDING"] = "sending";
    Status["WAITING"] = "waiting";
    Status["READY"] = "ready";
    Status["ERROR"] = "error";
})(Status = exports.Status || (exports.Status = {}));
/**
 * A FishbowlClient holds a socket connection to FB Server and manages queuing
 * and dispatching requests and responses. The client exposes a single async `send`
 * method that accepts a ClientRequest. The returned promise will receive the
 * text server response when fulfilled, or a rehydrated JSON object if the `json`
 * property on the request object was truthy.
 */
class FishbowlClient {
    constructor(_host = '127.0.0.1', _port = 28192) {
        this._host = _host;
        this._port = _port;
        this.status = Status.OFFLINE;
        this.reconnecting = false;
        // Outbound request queue.
        this.requests = [];
        // Initialize socket and events.
        this.connection = new net_1.default.Socket();
        this.connection.on('close', () => {
            logger_1.default.warn('Fishbowl Server offline.');
            this.status = Status.OFFLINE;
        });
        this.connection.on('error', (err) => {
            logger_1.default.warn('Fishbowl Server connection error, resetting...');
            if (!this.reconnecting) {
                this.reconnecting = true;
                this.connection.emit('close');
                this._connect();
            }
            else {
                this.status = Status.ERROR;
                logger_1.default.error('Terminal connection error, Fishbowl Server is not network accessible.');
                logger_1.default.error(err);
                this.connection.end();
            }
        });
        this._connect();
    }
    /**
     * The only public method. Call with a ClientRequest to receive a Promise awaiting
     * the text response from the FB Server.
     */
    send(request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!request.json && typeof request.body !== 'string') {
                request.json = true;
            }
            return new Promise((resolve, reject) => {
                this.requests.push({
                    request,
                    resolve,
                    reject
                });
                logger_1.default.info('Fishbowl request queued.');
                if (this.status !== Status.SENDING && this.status !== Status.WAITING) {
                    this._dispatch();
                }
            });
        });
    }
    /**
     * Open the socket to FB Server. The Promise returned fulfills in the callback to
     * opening the socket so we can await a confirmed connection before sending data.
     */
    _connect() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Connecting to Fishbowl Server...');
            if (this.status !== Status.OFFLINE) {
                return true;
            }
            this.status = Status.CONNECTING;
            return new Promise((resolve, reject) => {
                try {
                    let resLength;
                    let resData = Buffer.alloc(0);
                    // Connect to FB Server.
                    this.connection.connect(this._port, this._host, () => {
                        this.status = Status.READY;
                        logger_1.default.info('Fishbowl Server ready!');
                        resolve();
                    });
                    // Set up response listener.
                    this.connection.on('data', data => {
                        logger_1.default.info('Fishbowl Server sent data!');
                        if (resLength === undefined) {
                            resLength = data.readInt32BE(0);
                            resData = data.slice(4);
                        }
                        else {
                            resData = Buffer.concat([resData, data]);
                        }
                        // Check for end of response and if found, emit.
                        if (resData.length === resLength) {
                            const resText = resData.toString('utf8');
                            resLength = undefined;
                            this.connection.emit('done', null, resText);
                        }
                    });
                }
                catch (e) {
                    reject(e);
                }
            });
        });
    }
    /**
     * Dispatches a single request from the queue.
     */
    _dispatch() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            logger_1.default.info('Dispatching queued Fishbowl request...');
            const next = this.requests.shift();
            if (next === undefined) {
                logger_1.default.info('No queued requests found.');
                return;
            }
            const { request, resolve, reject } = next;
            if (this.status === Status.ERROR) {
                logger_1.default.error('Rejecting queued request because FB server is unreachable.');
                reject(new Error('Destination Fishbowl Server is unreachable.'));
                this._dispatch();
                return;
            }
            // Make sure the connection is active and ready.
            yield this._connect();
            this.status = Status.SENDING;
            // Set up response resolver.
            this.connection.once('done', (err, resText) => {
                logger_1.default.info('Fishbowl Server finished sending data.');
                this.status = Status.READY;
                if (err) {
                    logger_1.default.error('Unable to receive response: ');
                    logger_1.default.error(err);
                    return reject(err);
                }
                try {
                    resolve(request.json ? JSON.parse(resText) : resText);
                }
                catch (e) {
                    reject(e);
                }
                // Check for pending requests and if there are any, let's keep going.
                if (this.requests.length) {
                    process.nextTick(() => this._dispatch());
                }
            });
            // Send the request.
            let data = request.body;
            if (typeof data !== 'string') {
                data = request.json ? JSON.stringify(data) : data.toString();
            }
            const reqLength = Buffer.alloc(4);
            reqLength.writeIntBE(Buffer.byteLength(data, 'utf8'), 0, 4);
            logger_1.default.info('Sending request to Fishbowl Server...');
            this.connection.write(reqLength);
            this.connection.write(data);
            logger_1.default.info('Sent. Awaiting Fishbowl Server response...');
            this.status = Status.WAITING;
        });
    }
}
exports.FishbowlClient = FishbowlClient;
//# sourceMappingURL=client.js.map