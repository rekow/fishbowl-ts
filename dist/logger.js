"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const winston_1 = tslib_1.__importStar(require("winston"));
const logform = winston_1.format.combine(winston_1.format.label({ label: 'fishbowl' }), winston_1.format.timestamp({ format: 'MM/DD/YYYY hh:mm:ssA' }), winston_1.format.printf(({ level, label, message, timestamp }) => {
    return `${timestamp} [${label}] ${level.toUpperCase()}: ${message}`;
}));
const logger = winston_1.default.createLogger({
    format: logform,
    transports: []
});
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston_1.default.transports.Console({
        format: logform
    }));
}
exports.default = logger;
//# sourceMappingURL=logger.js.map