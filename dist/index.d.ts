import { Requests } from './requests';
import { FBResponse, Responses } from './responses';
/**
 * Catch-all class for any error from Fishbowl.
 */
export declare class FishbowlError {
    statusCode: number;
    message: string;
    param: string;
    static template: string;
    static statusCodes: Record<string, string>;
    constructor(statusCode: number, message?: string, param?: string);
    toString(): string;
}
/**
 * The configuration interface for the Fishbowl() constructor.
 */
export interface FishbowlConfig {
    host?: string;
    port?: number;
    iaId?: number;
    iaName?: string;
    iaDescription?: string;
    username?: string;
    password?: string;
    log?: boolean;
}
export default class Fishbowl {
    private static OK;
    private client;
    private key;
    private userId;
    private iaId;
    private iaName;
    private iaDescription;
    private password;
    private _authenticated;
    username: string;
    constructor({ iaId, iaName, iaDescription, username, password, host, port, }: FishbowlConfig);
    /**
     * Getter for login status.
     */
    get authenticated(): boolean;
    /**
     * Import data into Fishbowl.
     */
    import(request: Requests.Import): Promise<FBResponse>;
    /**
     * Get headers for a specific import.
     */
    getImportHeaders(request: Requests.ImportHeaders): Promise<Responses.ImportHeaders>;
    /**
     * Issue an SO.
     */
    issueSO(request: Requests.SORequest): Promise<FBResponse>;
    /**
     * Log into Fishbowl.
     */
    login(): Promise<Responses.Login | undefined>;
    /**
     * Log out of Fishbowl.
     */
    logout(): Promise<any>;
    /**
     * Run a SQL query and get CSV back.
     */
    query(request: Requests.Query): Promise<Responses.Query>;
    /**
     * Submit a quickship (pick, pack, ship) for an SO.
     */
    quickshipSO(request: Requests.QuickshipSO): Promise<FBResponse>;
    /**
     * Void out an SO.
     */
    voidSO(request: Requests.SORequest): Promise<FBResponse>;
    /**
     * Convenience method for dispatching to the client and throwing any errors
     * that occur during the request.
     * @private
     */
    private _sendRequest;
}
