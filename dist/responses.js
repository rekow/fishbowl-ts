"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Responses = void 0;
var Responses;
(function (Responses) {
    let Name;
    (function (Name) {
        Name["QUERY"] = "ExecuteQueryRs";
        Name["IMPORT"] = "ImportRs";
        Name["IMPORT_HEADERS"] = "ImportHeaderRs";
        Name["ISSUE_SO"] = "IssueSORs";
        Name["QUICKSHIP_SO"] = "QuickShipRs";
        Name["VOID_SO"] = "VoidSORs";
        Name["LOGIN"] = "LoginRs";
        Name["LOGOUT"] = "LogoutRs";
    })(Name = Responses.Name || (Responses.Name = {}));
})(Responses = exports.Responses || (exports.Responses = {}));
//# sourceMappingURL=responses.js.map