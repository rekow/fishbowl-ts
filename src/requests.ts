/**
 * Fishbowl-specific request interface declarations. Only used directly
 * in comms with FB server.
 */
interface FBQuery {
  Name: string;
  Query: string;
}

interface FBImport {
  Type: string;
  Rows: {
    Row: string[];
  }
}

interface FBHeaders {
  Type: string;
}

interface FBSalesOrder {
  SONumber: string;
}

interface FBQuickship extends FBSalesOrder {
  fulfillServiceItems: boolean;
  errorIfNotFulfilled: boolean;
  shipDate: string;
}

interface FBLogin {
  IAID: number;
  IAName: string;
  IADescription: string;
  UserName: string;
  UserPassword: string;
}

/**
 * Union type for all request objects sent directly to FB Server.
 * Logout requests are just an empty string.
 */
export type FBRequest = FBQuery | FBImport | FBHeaders | FBSalesOrder | FBQuickship | FBLogin | string;

/**
 * Interfaces for the request objects our API accepts.
 */
export namespace Requests {
  export interface Query {
    name?: string;
    query?: string;
  }

  export interface Import {
    type: string;
    row: Record<string, unknown>[];
  }

  export interface ImportHeaders {
    type: string;
  }

  export interface SORequest {
    soNumber: string;
  }

  export interface QuickshipSO extends SORequest {
    fulfillServices?: boolean;
    mustFulfill?: boolean;
    shipDate: string;
  }

  export enum Name {
    QUERY = 'ExecuteQueryRq',
    IMPORT = 'ImportRq',
    IMPORT_HEADERS = 'ImportHeaderRq',
    ISSUE_SO = 'IssueSORq',
    QUICKSHIP_SO = 'QuickShipRq',
    VOID_SO = 'VoidSORq',
    LOGIN = 'LoginRq',
    LOGOUT = 'LogoutRq',
  }
}


/**
 * The request envelope JSON format for a Fishbowl request. Exported but should
 * not be used directly by the library consumer.
 */
export class FBRequestEnvelope {
  FbiJson: {
    Ticket: {
      Key: string
    },

    FbiMsgsRq: Record<string, FBRequest>
  };

  constructor(key: string, request: Record<string, FBRequest>) {
    this.FbiJson = {
      Ticket: {
        Key: key
      },
      FbiMsgsRq: request
    };
  }
}
