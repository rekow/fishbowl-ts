import net from 'net';
import logger from './logger';

/**
 * A generic interface for an inbound request to a FishbowlClient.
 */
namespace FishbowlClient {
  export interface Request {
    body?: any;
    json?: boolean;
  }
}

/**
 * Intermediary object for associating requests and their promise resolvers.
 * Represents a pending request to FB Server.
 */
interface QueuedRequest {
  request: FishbowlClient.Request;
  resolve(value: any): void;
  reject(value: any): void;
}

/**
 * All possible states for the connection to FB Server.
 */
export enum Status {
  OFFLINE = 'offline',
  CONNECTING = 'connecting',
  SENDING = 'sending',
  WAITING = 'waiting',
  READY = 'ready',
  ERROR = 'error'
}

/**
 * A FishbowlClient holds a socket connection to FB Server and manages queuing
 * and dispatching requests and responses. The client exposes a single async `send`
 * method that accepts a ClientRequest. The returned promise will receive the
 * text server response when fulfilled, or a rehydrated JSON object if the `json`
 * property on the request object was truthy.
 */
export class FishbowlClient {
  status: Status = Status.OFFLINE;

  private requests: Array<QueuedRequest>;
  private connection: net.Socket;

  private reconnecting = false;

  constructor(
    private _host = '127.0.0.1',
    private _port = 28192
  ) {
    // Outbound request queue.
    this.requests = [];

    // Initialize socket and events.
    this.connection = new net.Socket();

    this.connection.on('close', () => {
      logger.warn('Fishbowl Server offline.');
      this.status = Status.OFFLINE;
    });

    this.connection.on('error', (err) => {
      logger.warn('Fishbowl Server connection error, resetting...');
      
      if (!this.reconnecting) {
        this.reconnecting = true;
        this.connection.emit('close');
        this._connect();
      } else {
        this.status = Status.ERROR;
        logger.error('Terminal connection error, Fishbowl Server is not network accessible.');
        logger.error(err);
        this.connection.end();
      }
    });

    this._connect();
  }

  /**
   * The only public method. Call with a ClientRequest to receive a Promise awaiting
   * the text response from the FB Server.
   */
  async send(request: FishbowlClient.Request): Promise<any> {
    if (!request.json && typeof request.body !== 'string') {
      request.json = true;
    }
    
    return new Promise((resolve, reject) => {
      this.requests.push({
        request,
        resolve,
        reject
      });

      logger.info('Fishbowl request queued.')

      if (this.status !== Status.SENDING && this.status !== Status.WAITING) {
        this._dispatch();
      }
    });
  }

  /**
   * Open the socket to FB Server. The Promise returned fulfills in the callback to
   * opening the socket so we can await a confirmed connection before sending data.
   */
  private async _connect()  {
    logger.info('Connecting to Fishbowl Server...');

    if (this.status !== Status.OFFLINE) {
      return true;
    }

    this.status = Status.CONNECTING;

    return new Promise((resolve, reject) => {
      try {
        let resLength: number | undefined;
        let resData: Buffer = Buffer.alloc(0);

        // Connect to FB Server.
        this.connection.connect(this._port, this._host, () => {
          this.status = Status.READY;
          logger.info('Fishbowl Server ready!');
          resolve();
        });

        // Set up response listener.
        this.connection.on('data', data => {
          logger.info('Fishbowl Server sent data!');

          if (resLength === undefined) {
            resLength = data.readInt32BE(0);
            resData = data.slice(4);
          } else {
            resData = Buffer.concat([resData, data]);
          }

          // Check for end of response and if found, emit.
          if (resData.length === resLength) {
            const resText = resData.toString('utf8');
            resLength = undefined;

            this.connection.emit('done', null, resText);
          }
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Dispatches a single request from the queue.
   */
  private async _dispatch() {
    logger.info('Dispatching queued Fishbowl request...');

    const next = this.requests.shift();

    if (next === undefined) {
      logger.info('No queued requests found.')
      return;
    }

    const { request, resolve, reject } = next;

    if (this.status === Status.ERROR) {
      logger.error('Rejecting queued request because FB server is unreachable.');
      reject(new Error('Destination Fishbowl Server is unreachable.'));
      this._dispatch();
      return;
    }

    // Make sure the connection is active and ready.
    await this._connect();

    this.status = Status.SENDING;

    // Set up response resolver.
    this.connection.once('done', (err, resText) => {
      logger.info('Fishbowl Server finished sending data.');

      this.status = Status.READY;

      if (err) {
        logger.error('Unable to receive response: ');
        logger.error(err);
        return reject(err);
      }

      try {
        resolve(request.json ? JSON.parse(resText) : resText);
      } catch (e) {
        reject(e);
      }

      // Check for pending requests and if there are any, let's keep going.
      if (this.requests.length) {
        process.nextTick(() => this._dispatch());
      }

    });

    // Send the request.
    let data = request.body;

    if (typeof data !== 'string') {
      data = request.json ? JSON.stringify(data) : data.toString();
    }

    const reqLength = Buffer.alloc(4);
    reqLength.writeIntBE(Buffer.byteLength(data, 'utf8'), 0, 4);

    logger.info('Sending request to Fishbowl Server...');

    this.connection.write(reqLength);
    this.connection.write(data);

    logger.info('Sent. Awaiting Fishbowl Server response...');

    this.status = Status.WAITING;
  }
}
