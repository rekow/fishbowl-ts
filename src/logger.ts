import winston, { format } from 'winston';

const logform = format.combine(
  format.label({ label: 'fishbowl' }),
  format.timestamp({ format: 'MM/DD/YYYY hh:mm:ssA'}),
  format.printf(({ level, label, message, timestamp }) => {
    return `${timestamp} [${label}] ${level.toUpperCase()}: ${message}`;
  })
);

const logger = winston.createLogger({
  format: logform,
  transports: []
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: logform
  }));
}

export default logger;