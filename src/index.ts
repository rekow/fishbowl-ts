import crypto from 'crypto';
import parse from 'csv-parse';
import stringify from 'csv-stringify';

import statusCodes from './static/fishbowl-status.json';
import logger from './logger';
import { FishbowlClient } from './client';
import { FBRequest, Requests, FBRequestEnvelope } from './requests';
import { FBResponse, Responses, FBResponseEnvelope } from './responses';


/**
 * Catch-all class for any error from Fishbowl.
 */
export class FishbowlError {
  static template = '_________';
  static statusCodes: Record<string, string> = statusCodes;

  constructor(
    public statusCode: number,
    public message = '',
    public param = ''
  ) {}

  toString(): string {
    const repr = `<${this.statusCode}>: ${this.message}`;
    return repr.replace(FishbowlError.template, this.param);
  }
}

/**
 * The configuration interface for the Fishbowl() constructor.
 */
export interface FishbowlConfig {
  host?: string;
  port?: number;
  iaId?: number;
  iaName?: string;
  iaDescription?: string;
  username?: string;
  password?: string;
  log?: boolean
}

export default class Fishbowl {
  private static OK = 1000;

  private client: FishbowlClient;

  private key = '';
  private userId = 0;
  private iaId: number;
  private iaName: string;
  private iaDescription: string;
  private password: string;
  private _authenticated = false;
  
  username: string;

  constructor({
    iaId = 54321,
    iaName = 'fishbowlTS',
    iaDescription = 'fishbowlTS helper',
    username = 'admin',
    password = 'admin',
    host,
    port,
  }: FishbowlConfig) {
    this.iaId = iaId;
    this.iaName = iaName;
    this.iaDescription = iaDescription;
    this.username = username;
    this.password = password;

    this.client = new FishbowlClient(host, port);
  }

  /**
   * Getter for login status.
   */
  get authenticated(): boolean {
    return this._authenticated;
  }

  /**
   * Import data into Fishbowl.
   */
  async import(request: Requests.Import): Promise<FBResponse> {
    logger.info('Got import() request.');

    if (!this._authenticated) {
      await this.login();
    }

    const row = [];
    const stringifier = stringify({
      header: true
    });

    stringifier.write(request.row);
    stringifier.end();

    for await (const csvRecord of stringifier) {
      row.push(csvRecord);
    }

    const response = await this._sendRequest(
      Requests.Name.IMPORT,
      Responses.Name.IMPORT,
      {
        Type: request.type,
        Rows: {
          Row: row
        }
      });

    logger.info('Import successful! Got response from Fishbowl Server:')
    logger.info(JSON.stringify(response));
    return response;
  }
  /**
   * Get headers for a specific import.
   */
  async getImportHeaders(request: Requests.ImportHeaders): Promise<Responses.ImportHeaders> {
    logger.info('Got getImportHeaders() request.');

    if (!this._authenticated) {
      await this.login();
    }

    let response = await this._sendRequest(
      Requests.Name.IMPORT_HEADERS,
      Responses.Name.IMPORT_HEADERS,
      {
        Type: request.type
      }) as Responses.ImportHeaders;

    logger.info('Import headers request successful! Got response from Fishbowl Server:');
    logger.info(JSON.stringify(response));

    const parser = parse();
    parser.write(response.Header.Row.join('\n'));
    parser.end();

    const headers = [];
    for await (const header of parser) {
      headers.push(header);
    }

    response.Header.Row = headers;
    return response;
  }

  /**
   * Issue an SO.
   */
  async issueSO(request: Requests.SORequest): Promise<FBResponse> {
    logger.info('Got issueSO() request.');

    if (!this._authenticated) {
      await this.login();
    }

    const response = await this._sendRequest(
      Requests.Name.ISSUE_SO,
      Responses.Name.ISSUE_SO,
      {
        SONumber: request.soNumber
      });

    logger.info('Issued SO successfully. Got response from Fishbowl Server:');
    logger.info(JSON.stringify(response));
    return response;
  }

  /**
   * Log into Fishbowl.
   */
  async login(): Promise<Responses.Login | undefined> {
    logger.info('Got login() request.');

    if (this._authenticated) {
      logger.info('Already logged in!');
      return;
    }

    const response = await this._sendRequest(
      Requests.Name.LOGIN,
      Responses.Name.LOGIN,
      {
        IAID: this.iaId,
        IAName: this.iaName,
        IADescription: this.iaDescription,
        UserName: this.username,
        UserPassword: crypto
          .createHash('md5')
          .update(this.password)
          .digest('base64')
      }) as Responses.Login;

    logger.info('Login successful! Got response from Fishbowl Server:');
    logger.info(JSON.stringify(response));

    this._authenticated = true;

    return response;
  }

  /**
   * Log out of Fishbowl.
   */
  async logout(): Promise<any> {
    logger.info('Got logout() request.');

    const response = await this._sendRequest(
      Requests.Name.LOGOUT,
      Responses.Name.LOGOUT,
      '');

    logger.info('Logout successful.');

    this._authenticated = false;
    return response;
  }

  /**
   * Run a SQL query and get CSV back.
   */
  async query(request: Requests.Query): Promise<Responses.Query> {
    const query = request.query || '';

    logger.info('Got query() request.');

    if (!this._authenticated) {
      await this.login();
    }

    const response = await this._sendRequest(
      Requests.Name.QUERY,
      Responses.Name.QUERY,
      {
        Name: request.name || '',
        Query: query
      }) as Responses.Query;
 
    logger.info('Query successful!');

    // Convert any returned CSV to JSON objects keyed by column headers.
    try {
      logger.info('Got CSV response from Fishbowl Server. Converting to JSON...');

      const parser = parse({
        columns: true   // `true` will use the first row as headers and map data as objects
      });

      parser.write(response.Rows.Row.join('\n'));
      parser.end();

      const rows = [];
      for await (const record of parser) {
        rows.push(record);
      }

      logger.info('CSV successfully converted to JSON.');

      response.Rows.Row = rows;
    } catch (e) {
      logger.warn('Error parsing CSV to JSON: ');
      logger.warn(JSON.stringify(e));
    }

    return response;
  }

  /**
   * Submit a quickship (pick, pack, ship) for an SO.
   */
  async quickshipSO(request: Requests.QuickshipSO): Promise<FBResponse> {
    logger.info('Got quickshipSO() request.');

    if (!this._authenticated) {
      await this.login();
    }

    const response = await this._sendRequest(
      Requests.Name.QUICKSHIP_SO,
      Responses.Name.QUICKSHIP_SO,
      {
        SONumber: request.soNumber,
        shipDate: request.shipDate,
        fulfillServiceItems: !!request.fulfillServices,
        errorIfNotFulfilled: !!request.mustFulfill
      });

    logger.info('Quickship successful! Got response from Fishbowl Server:');
    logger.info(JSON.stringify(response));

    return response;
  }

  /**
   * Void out an SO.
   */
  async voidSO(request: Requests.SORequest): Promise<FBResponse> {
    logger.info('Got voidSO() request.');

    if (!this._authenticated) {
      await this.login();
    }

    const response = await this._sendRequest(
      Requests.Name.VOID_SO,
      Responses.Name.VOID_SO,
      {
        SONumber: request.soNumber
      });

    logger.info('Voided SO successfully. Got response from Fishbowl Server: ');
    logger.info(JSON.stringify(response));

    return response;
  }

  /**
   * Convenience method for dispatching to the client and throwing any errors
   * that occur during the request.
   * @private
   */
  private async _sendRequest(
    type: Requests.Name,
    responseType: Responses.Name,
    data: FBRequest
  ): Promise<FBResponse> {
    logger.info(`Sending ${type} request to Fishbowl Server...`);
  
    const body = new FBRequestEnvelope(this.key, {
      [type]: data
    });

    let response: FBResponseEnvelope = await this.client.send({ body });
    const json = response.FbiJson;

    if (!this.key) {
      this.key = json.Ticket.Key;
    }

    if (!this.userId) {
      this.userId = json.Ticket.UserID;
    }

    logger.info('Response received from Fishbowl Server.');

    // handle transport, auth or server-level errors
    let status = json.FbiMsgsRs.statusCode;
    logger.info(`Server status came back as ${status}.`)

    if (status !== Fishbowl.OK) {
      logger.error(`Fishbowl server error: ${status}`);
      throw new FishbowlError(
        status,
        json.FbiMsgsRs?.statusMessage || FishbowlError.statusCodes[status],
        type
      );
    }

    const responseData = json.FbiMsgsRs[responseType] as FBResponse;
    status = responseData.statusCode;
    logger.info(`Request status came back as ${status}.`)

    if (status !== Fishbowl.OK) {
      logger.error(`Fishbowl request error: ${status}`);
      throw new FishbowlError(
        status,
        responseData.statusMessage || FishbowlError.statusCodes[status],
        type
      );
    }

    return responseData;
  }
}
